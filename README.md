This script I made to pull MMS (both group and images) using AT&T and ModemManager. It will also email the MMS message and images to you.
It is based off of the script by 5ilver (link to his script in the comments at the top of the script), with the addition of the group messages and emails, and converted for AT&T.

I just whipped this up over the last hour, so you may find issues, but so far all of my tests it works fine.
Handles multiple images, single images, group text, group with images, etc. Standard SMS will still be handled by Chatty or whatever you use for SMS.

The email will contain the phone numbers of the people involved in the subject. The body will have any text from the message, and any jpegs will be attached to the email.

If you dont want to get emails, then comment out the s-nail line. The images will be dumped into foldes in your home folder. The text of the mesage will be displayed in the output.

For fedora, install s-nail or mailx and recoverjpeg
For mobian, install s-nail, and libnotify-bin, and recoverjpeg
For Arch, i assume the packages are named the same, and I think your mailx works like the fedora one. But i dint test arch.

Since i just wrote it, i am not yet cleaning up the files it writes, as these may be needed for debugging. 

This will also resolve the issue with Chatty locking up and not getting further SMS messages after an MMS is received.

It is recommended that you read and understand the script so you can troubleshoot. It should also work for other carriers, but you will need to customize the curl line for them.

I only tested this using the AT&T NXTGENPHONE APN

