#!/bin/bash

#This is based on the tmobile image getting version by 5ilver, aka silver, https://gitlab.com/5ilver/silvermms . I dont know his real name unfortuneatly to list it for the credit of his initial work
#Converted to AT&T and to get group messages with text, and email them 
#This version by David Haltinner 

#########################################################

#FILL THIS IN IF YOU WANT EMAILS
SMTPSERVER=example.org
FROMADD=mms@example.org
TOADD=duh@example.org

#Specify where to store the files
DIRTOUSE=/home/mobian/mms/

#If you want it to make sound when it gets a new message, set this
SOUND=/usr/share/sounds/gnome/default/alerts/drip.ogg
##########################################################

mkdir -p /home/mobian/mms/
for modem in {0..100}; do mmcli -m $modem --messaging-list-sms && break; done
mmcli -m $modem --messaging-list-sms | cut -f 6 -d '/' | grep received | awk '{print $1}' | while read smsnum
do 
  mmcli -m $modem -s $smsnum --create-file-with-data=$DIRTOUSE$smsnum.data.sms 
  mmcli -m $modem -s $smsnum 
  i=0
  cat $DIRTOUSE$smsnum.data.sms | tr -c '[:print:]' '\n' | grep http | while read file
  do 
   #We need to specify the interface and proxy for AT&T. it works when wifi is connected too
   echo "New MMS URL: $file"

   echo "$file" >> /home/mobian/.retrieveMMS

  done
  mmcli -m $modem --messaging-delete-sms $smsnum; 
done

#We cant retrieve them without a wwan IP address
IP=$(ip a show dev wwan0 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1)
if [ -z "$IP" ] ; then
  echo "I have no cellular IP address! I need one for AT&T"
  #TODO: add reconnect to cellular data
  #notify-send "No IP to retrieve MMS!"
  #ogg123 $SOUND &> /dev/null
  exit
else
  for file in $(cat /home/mobian/.retrieveMMS) ; do
    i=$(date +%s)
    NEWFILE="$DIRTOUSE$i"
    curl --interface wwan0 --proxy http://proxy.mobile.att.net:80 "$file" -o $NEWFILE
    EXITCODE=$?
    if [[ $EXITCODE > 0 ]] ; then
     ip a | s-nail -s "FAILED TO DOWNLOAD New MMS message $file" -S smtp=$SMTPSERVER -r $FROMADD $TOADD
     echo "Failed to download MMS message $file"
     ip a
    else
      mkdir $NEWFILE.out
      #we want to use the output, the # of recovered jpegs, but the thing sends that to stderr? why?
      recoverjpeg -b1 $NEWFILE -o $NEWFILE.out
      echo "recoverjpeg status code: $?"
 
      #Now we will check for text included with the image, OR if it was only a group message with text, this will grab it too
      LASTLINE=$(tail -n 1 $NEWFILE)
      TEXT=
      HASIMAGE=
      PEOPLE=
      for line in $(head -n 1 $NEWFILE | grep -a -o -E '[0-9]{10}/') ; do 
        PEOPLE="$PEOPLE ${line%/}" 
      done
      #Sometimes they come as 11 digit numbers, so we'll just be brutes and do it again, 
      #might get some garbage numbers but we will work through that later
      for line in $(head -n 1 $NEWFILE | grep -a -o -E '[0-9]{11}/') ; do 
        PEOPLE="$PEOPLE ${line%/}"
      done

      if [[ $LASTLINE == *"http://"* ]] ; then 
        TEXT=$(echo $LASTLINE | awk -F"section=1.1" '{print $NF}')
      elif [[ $LASTLINE == *".txt"* ]] ; then
        TEXT=$(echo $LASTLINE | awk -F".txt" '{print $NF}')
        for line in $(find $NEWFILE.out/ -type f) ; do HASIMAGE="$HASIMAGE -a $line" ; done
      else
        for line in $(find $NEWFILE.out/ -type f) ; do HASIMAGE="$HASIMAGE -a $line" ; done
      fi
      echo "Image(?): $HASIMAGE"
      echo "Message: $TEXT"
      notify-send "MMS:$TEXT :Has Image? $HASIMAGE"

      #Im used to the fedora/rh/centos mailx which i can use a -S for options, not sure why mobian doesnt let us, 
      #so this is a mobian workaround. Arch users, i assume this works for too but didnt try
      echo $TEXT | s-nail $HASIMAGE -s "New MMS message with $PEOPLE" -S smtp=$SMTPSERVER -r $FROMADD $TOADD
      if [[ $? -ne 0 ]] ; then 
        echo "FAILED TO SEND EMAIL" 
      else 
        echo "email sent to $SMTPSERVER"
        sed -i "/$(echo $file | awk -F\/ '{print $NF}')/d" /home/mobian/.retrieveMMS
      fi
      if [[ 10#$(date +%H) -lt 21 ]] && [[ 10#$(date +%H) -gt 07 ]] ; then
        ogg123 $SOUND &> /dev/null
      fi
    fi
  done
fi
